package me.Lorinth.RpWarps;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class WarpWindow {

	private Inventory inv;
	private Player play;
	private RpWarpsMain main;
	private HashMap<Integer, Warp> windowWarps = new HashMap<Integer, Warp>();
	private PlayerWarpProfile profile;
	private ItemStack page1;
	private ItemStack page2;
	private ItemStack page3;
	private ItemStack page4;
	private ItemStack page5;
	private ItemStack page6;
	private ItemStack page7;
	private ItemStack page8;
	private ItemStack page9;
	private ItemStack blank;
	private int pageNumber;
	private int warpIconsPlaced;
	
	public WarpWindow(Player p, RpWarpsMain main, int pageIn){
		this.main = main;
		play = p;
		profile = main.profiles.get(p);
		pageNumber = pageIn;
		
		if(play.hasPermission("LRWarps.moreWarps.1")){
			createPage1();
			createPage2();
			createPage3();
			createPage4();
			createPage5();
			createPage6();
			createPage7();
			createPage8();
			createPage9();
		}
		createBlank();
		createWindow();
	}
	
	private void createPage1(){
		page1 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) main.configYml.getInt("page 1 Icon Color"));
		ItemMeta meta = page1.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', main.configYml.getString("Page 1 Text Color")) + "page1");
		page1.setItemMeta(meta);
	}
	
	private void createPage2(){
		page2 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) main.configYml.getInt("page 2 Icon Color"));
		ItemMeta meta = page2.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', main.configYml.getString("Page 2 Text Color")) + "page2");
		page2.setItemMeta(meta);
	}
	
	private void createPage3(){
		page3 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) main.configYml.getInt("page 3 Icon Color"));
		ItemMeta meta = page3.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', main.configYml.getString("Page 3 Text Color")) + "page3");
		page3.setItemMeta(meta);
	}
	
	private void createPage4(){
		page4 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) main.configYml.getInt("page 4 Icon Color"));
		ItemMeta meta = page4.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', main.configYml.getString("Page 4 Text Color")) + "page4");
		page4.setItemMeta(meta);
	}
	
	private void createPage5(){
		page5 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) main.configYml.getInt("page 5 Icon Color"));
		ItemMeta meta = page5.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', main.configYml.getString("Page 5 Text Color")) + "page5");
		page5.setItemMeta(meta);
	}
	
	private void createPage6(){
		page6 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) main.configYml.getInt("page 6 Icon Color"));
		ItemMeta meta = page6.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', main.configYml.getString("Page 6 Text Color")) + "page6");
		page6.setItemMeta(meta);
	}
	
	private void createPage7(){
		page7 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) main.configYml.getInt("page 7 Icon Color"));
		ItemMeta meta = page7.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', main.configYml.getString("Page 7 Text Color")) + "page7");
		page7.setItemMeta(meta);
	}
	
	private void createPage8(){
		page8 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) main.configYml.getInt("page 8 Icon Color"));
		ItemMeta meta = page8.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', main.configYml.getString("Page 8 Text Color")) + "page8");
		page8.setItemMeta(meta);
	}
	
	private void createPage9(){
		page9 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) main.configYml.getInt("page 9 Icon Color"));
		ItemMeta meta = page9.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', main.configYml.getString("Page 9 Text Color")) + "page9");
		page9.setItemMeta(meta);
	}
	
	private void createBlank(){
		blank = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) main.configYml.getInt("blank Color"));
		ItemMeta meta = blank.getItemMeta();
		meta.setDisplayName(" ");
		blank.setItemMeta(meta);
	}

	private void createWindow(){
		play.sendMessage(ChatColor.GREEN + "Opening teleport menu!");
		if(play.hasPermission("LRWarps.moreWarps.2") && main.configYml.getBoolean("Show Page Number")){
			inv = Bukkit.createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', main.configYml.getString("Title With number Color")) + main.configYml.getString("Title With number") + ChatColor.translateAlternateColorCodes('&', main.configYml.getString("Page Number Color")) + pageNumber);
		} else {
			inv = Bukkit.createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', main.configYml.getString("Title Color")) + main.configYml.getString("Title"));
		}

		int pos = 0;
		
		for(Warp w : main.serverWarps){
			inv.setItem(pos, w.display);
			windowWarps.put(pos, w);
			pos++;
		}
		if(pos <= 8){
			pos = 9;
		}
		
		for (int i = 0; i < 9; i++){
			if(play.hasPermission("LRWarps.moreWarps." + (i + 1))){
				if(i == 0){
					inv.setItem(pos, page1);
				}
				if(i == 1){
					inv.setItem(pos + 1, page2);
				}
				if(i == 2){
					inv.setItem(pos + 2, page3);
				}
				if(i == 3){
					inv.setItem(pos + 3, page4);
				}
				if(i == 4){
					inv.setItem(pos + 4, page5);
				}
				if(i == 5){
					inv.setItem(pos + 5, page6);
				}
				if(i == 6){
					inv.setItem(pos + 6, page7);
				}
				if(i == 7){
					inv.setItem(pos + 7, page8);
				}
				if(i == 8){
					inv.setItem(pos + 8, page9);
				}
			} else {
				inv.setItem(pos + i, blank);
			}

		}

		pos += 9;
		warpIconsPlaced = 0;
		
		ArrayList<String> remove = new ArrayList<String>();
		ArrayList<Warp> bannedFrom = new ArrayList<Warp>();
		for(String id : profile.knownWarps){
			play.sendMessage(ChatColor.GREEN + id);
			Warp w = Data.loadWarp(id);
			if(w != null){
				if(w.bannedUsers.contains(play.getUniqueId().toString())){
					bannedFrom.add(w);
				}
				else{
					if((warpIconsPlaced >= (36 * (pageNumber - 1))) && (warpIconsPlaced < (36 * pageNumber))){
						inv.setItem(pos, w.display);
						windowWarps.put(pos, w);
						pos++;
					}
					warpIconsPlaced++;
				}
			}
			else{
				remove.add(id);
			}
			
		}
		for(Warp w : bannedFrom){
			//Remove known warp from user profile
			profile.knownWarps.remove(w);
		}
		profile.knownWarps.removeAll(remove);
		
		play.openInventory(inv);
	}
	
	void HandleClickEvent(InventoryClickEvent event){
		if(inv.getSize() > event.getSlot() && event.getSlot() >= 0){
			ItemStack item = inv.getItem(event.getSlot());
			if(item != null){
				if(event.getSlot() < 9 || 17 < event.getSlot()){		
					play.closeInventory();
					
					Warp w = windowWarps.get(event.getSlot());
					if(w.ServerOwned){
						w.teleport(play);
						play.sendMessage(ChatColor.GREEN + "You have warped to " + w.name + ChatColor.GREEN + "!");
				
					}
					else{
						main.playerConfirms.put(play, new WarpConfirmWindow(main, play, w, profile));
					}
				} else {
					int i = (event.getSlot() - 8);
					if(play.hasPermission("LRWarps.moreWarps." + i)){
						play.closeInventory();
						WarpWindow win = new WarpWindow(play, main, i);
						main.warpWindows.put(play, win);
					}
				}
			}
		}
	}	
}
