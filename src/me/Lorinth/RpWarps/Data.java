package me.Lorinth.RpWarps;

import java.io.File;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;

public class Data extends RpWarpsMain{
	
	@SuppressWarnings("null")
	public static Warp loadWarp(String ID){
		Warp retVal = null;

		YamlConfiguration warpFile;
		String warpFileName = ID + ".Yml";
		String dir = ("plugins/RpWarps/warps/");
		File warpID = new File(dir + warpFileName);
		warpFile = YamlConfiguration.loadConfiguration(warpID);
		try{
			if(warpID.exists()){
				retVal.owner = warpFile.getString(".Owner");
				retVal.name = warpFile.getString(".Name");
				retVal.mat = Material.getMaterial(warpFile.getString(".Material"));	
				
				String worldname = warpFile.getString(".Location.World");
				double x, y, z;
				x = warpFile.getDouble(".Location.X");
				y = warpFile.getDouble(".Location.Y");
				z = warpFile.getDouble(".Location.Z");
				retVal.loc = new Location(Bukkit.getWorld(worldname), x, y, z);
				
				if (warpFile.contains(".BannedUsers")) {
					retVal.bannedUsers = (ArrayList<String>) warpFile.getStringList(".BannedUsers");
				} else {
					retVal.bannedUsers = new ArrayList<String>();
				}
			}
		} catch(NullPointerException e){
		}
		
		return retVal;
	}
}