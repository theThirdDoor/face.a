Config
first line just marks that config is installed
for text colors, make sure you use an & symbol

added colored control for the row between 
serverwarps and playerwarps, no longer needs to
be white.

added buttons to reach extra screens of warps
based on perms

added autohiding of buttons to pages players 
can't fill

auto increases each player's maxwarps to the
number of pages * 36

permissions

added permissions LRWarps.moreWarps.1 to LRWarps.moreWarps.9
each permission adds the button of it's number,
with the exception of button 1, this is added 
by LRWarps.moreWarps.2 so players with only one
page get no buttons.

cleared depreciated methods
replaced cases of int += 1; with int++;